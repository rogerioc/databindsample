package com.rogerio.databindsample;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.rogerio.databindsample.databinding.ContentShowUserBinding;
import com.rogerio.databindsample.model.Login;
import com.rogerio.databindsample.model.ViewLogin;

public class ShowUser extends AppCompatActivity {

    public static final String KEY_LOGIN = "key_user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user);
        ContentShowUserBinding binding = DataBindingUtil.setContentView(this,R.layout.content_show_user);
        Intent intent = getIntent();
        if(intent!=null && intent.getParcelableExtra(KEY_LOGIN)!=null) {
            Login login = intent.getParcelableExtra(KEY_LOGIN);
            ViewLogin viewLogin = new ViewLogin(login);
            binding.setLoginview(viewLogin);
        }else {
            finish();
        }

    }

}
