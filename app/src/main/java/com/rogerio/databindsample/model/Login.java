package com.rogerio.databindsample.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rogerio on 30/01/17.
 */

public class Login implements Parcelable {
    private String email;
    private String password;

    public Login(String email, String password) {
        this.email= email;
        this.password=password;
    }

    public Login() {
    }


    public String getEmail() {
        return email;
    }

    public Login setEmail(String email) {
        this.email=email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Login setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeString(this.password);
    }

    protected Login(Parcel in) {
        this.email = in.readString();
        this.password = in.readString();
    }

    public static final Parcelable.Creator<Login> CREATOR = new Parcelable.Creator<Login>() {
        @Override
        public Login createFromParcel(Parcel source) {
            return new Login(source);
        }

        @Override
        public Login[] newArray(int size) {
            return new Login[size];
        }
    };
}
