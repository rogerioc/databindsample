package com.rogerio.databindsample.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;


import com.rogerio.databindsample.BR;

/**
 * Created by rogerio on 07/02/17.
 */

public class ViewLogin extends BaseObservable {
    Login login;

    public ViewLogin(Login login) {
        this.login = login;
    }

    @Bindable
    public String getEmail() {
        return login.getEmail();
    }

    public void setEmail(String email) {
        login.setEmail(email);
        notifyPropertyChanged(BR.email);
    }

    public void setPassword(String password) {
        login.setPassword(password);
    }
    @Bindable
    public String getPassword(){
        return login.getPassword();
    }
}
