package com.rogerio.databindsample;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.rogerio.databindsample.databinding.ActivityLoginBinding;
import com.rogerio.databindsample.model.Login;
import com.rogerio.databindsample.model.ViewLogin;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private Login login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        ActivityLoginBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        this.login = new Login("rogerio.celestino@gmai.com","1234");

        binding.setLoginview(new ViewLogin(login));

    }


    public void onClickSubmit(View view) {
        Intent intent = new Intent(this,ShowUser.class);
        intent.putExtra(ShowUser.KEY_LOGIN,login);
        startActivity(intent);
    }



}

